export interface Passenger {
  title: string;
  gender: string;
  firstName: string;
  lastName: string;
  dateOfBirth: string;
  [key: string]: string; // Index signature for additional properties
}

export interface PassengerFormProps {
  initialPassenger: Passenger;
  onUpdatePassenger: (updatedPassenger: Passenger) => void;
  editable: boolean;
  handleSelectPassenger: (event:any) => void;
  selectedPassengerKey : string
}

export interface ConfirmationPageProps {
  proposedPassenger: Passenger;
  onCancel: () => void;
  onConfirm: () => void;
}
