import { calculateEditDistance } from '../utils/utils';

describe('calculateEditDistance', () => {
  it('should return the correct edit distance for strings of equal length', () => {
    expect(calculateEditDistance('kitten', 'sitten')).toBe(1);
    expect(calculateEditDistance('sitting', 'sittong')).toBe(1);
    expect(calculateEditDistance('Saturday', 'Sundays')).toBe(4);
  });


  it('should return 0 for identical strings', () => {
    expect(calculateEditDistance('hello', 'hello')).toBe(0);
    expect(calculateEditDistance('', '')).toBe(0);
  });

  it('should handle empty strings', () => {
    expect(calculateEditDistance('', 'test')).toBe(4);
    expect(calculateEditDistance('test', '')).toBe(4);
    expect(calculateEditDistance('', '')).toBe(0);
  });
});
