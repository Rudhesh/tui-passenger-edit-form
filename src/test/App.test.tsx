import React from 'react';
import { render, fireEvent, screen, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import App from '../App';
import { Passenger } from '../types/types';



// Mock passengers data
jest.mock('../passengers.json', () => [
  {
    id: '1',
    title: 'MR',
    firstName: 'John',
    lastName: 'Doe',
    gender: 'MALE',
    dateOfBirth: '1990-01-01',
  },
]);

// Mock ConfirmationPage component
jest.mock('../components/ConfirmationPage', () => () => <div>Confirmation Page</div>);

// Mock PassengerForm component
jest.mock('../components/PassengerForm', () => ({ initialPassenger, onUpdatePassenger, editable, handleSelectPassenger, selectedPassengerKey }: { initialPassenger: Passenger, onUpdatePassenger: (updatedPassenger: Passenger) => void, editable: boolean, handleSelectPassenger: (event: React.ChangeEvent<HTMLSelectElement>) => void, selectedPassengerKey: string }) => (
    <div>
    Passenger Form
    <button onClick={() => onUpdatePassenger({ ...initialPassenger, firstName: 'Jane' })}>Update Passenger</button>
  </div>
));

describe('App', () => {
  

  test('updates passenger details when Update Passenger button is clicked', () => {
    const { getByText } = render(<LocalizationProvider dateAdapter={AdapterDayjs}>
        <App />
      </LocalizationProvider>)

    // Click the Update Passenger button
    fireEvent.click(getByText('Update Passenger'));
  
  });
});
