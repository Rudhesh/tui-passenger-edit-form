import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import PassengerForm from '../components/PassengerForm';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';

const mockInitialPassenger = {
  title: 'MR',
  firstName: 'John',
  lastName: 'Doe',
  gender: 'MALE',
  dateOfBirth: '1990-01-01',
};

test('renders PassengerForm with initial passenger details', () => {
  const mockOnUpdatePassenger = jest.fn();
  const mockHandleSelectPassenger = jest.fn();

  render(
    <LocalizationProvider dateAdapter={AdapterDayjs}>
    <PassengerForm
      initialPassenger={mockInitialPassenger}
      onUpdatePassenger={mockOnUpdatePassenger}
      editable={true}
      handleSelectPassenger={mockHandleSelectPassenger}
      selectedPassengerKey="0"
    />
    </LocalizationProvider>
  );

  // Check if all initial passenger details are rendered correctly
  expect(screen.getByDisplayValue('MR')).toBeInTheDocument();
  expect(screen.getByDisplayValue('John')).toBeInTheDocument();
  expect(screen.getByDisplayValue('Doe')).toBeInTheDocument();
  expect(screen.getByDisplayValue('MALE')).toBeInTheDocument();
  // expect(screen.getByDisplayValue('1990-01-01')).toBeInTheDocument();
});

test('calls onUpdatePassenger when update button is clicked', () => {
  const mockOnUpdatePassenger = jest.fn();
  const mockHandleSelectPassenger = jest.fn();

  render(
    <LocalizationProvider dateAdapter={AdapterDayjs}>
    <PassengerForm
      initialPassenger={mockInitialPassenger}
      onUpdatePassenger={mockOnUpdatePassenger}
      editable={true}
      handleSelectPassenger={mockHandleSelectPassenger}
      selectedPassengerKey="0"
    />
     </LocalizationProvider>
  );

  // Click the update button
  fireEvent.click(screen.getByRole('button', { name: /Update Details/i }));
  expect(mockOnUpdatePassenger).toHaveBeenCalledTimes(1);
});
