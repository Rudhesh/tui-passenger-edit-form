import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ConfirmationPage from '../components/ConfirmationPage';

const mockProposedPassenger = {
  title: 'Mr',
  firstName: 'John',
  lastName: 'Doe',
  gender: 'Male',
  dateOfBirth: '1990-01-01',
};

test('renders ConfirmationPage with correct details', () => {
  const onCancelMock = jest.fn();
  const onConfirmMock = jest.fn();

  render(
    <ConfirmationPage
      proposedPassenger={mockProposedPassenger}
      onCancel={onCancelMock}
      onConfirm={onConfirmMock}
    />
  );

  // Check if all details are displayed correctly
  expect(screen.getByText(/Please review your changes carefully/i)).toBeInTheDocument();
  expect(screen.getByText(/Title: Mr/i)).toBeInTheDocument();
  expect(screen.getByText(/First Name: John/i)).toBeInTheDocument();
  expect(screen.getByText(/Last Name: Doe/i)).toBeInTheDocument();
  expect(screen.getByText(/Gender: Male/i)).toBeInTheDocument();
  expect(screen.getByText(/Date of Birth: 1990-01-01/i)).toBeInTheDocument();

  // Check if buttons are rendered
  expect(screen.getByRole('button', { name: /Cancel/i })).toBeInTheDocument();
  expect(screen.getByRole('button', { name: /Confirm Changes/i })).toBeInTheDocument();
});

test('calls onCancel and onConfirm callbacks when corresponding buttons are clicked', () => {
  const onCancelMock = jest.fn();
  const onConfirmMock = jest.fn();

  render(
    <ConfirmationPage
      proposedPassenger={mockProposedPassenger}
      onCancel={onCancelMock}
      onConfirm={onConfirmMock}
    />
  );

  // Click cancel button
  fireEvent.click(screen.getByRole('button', { name: /Cancel/i }));
  expect(onCancelMock).toHaveBeenCalledTimes(1);

  // Click confirm button
  fireEvent.click(screen.getByRole('button', { name: /Confirm Changes/i }));
  expect(onConfirmMock).toHaveBeenCalledTimes(1);
});
