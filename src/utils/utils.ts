export const calculateEditDistance = (s1: string, s2: string) => {
    const len1 = s1.length;
    const len2 = s2.length;
    const dp: number[][] = [];
  
    // Initialize the dynamic programming table
    for (let i = 0; i <= len1; i++) {
      dp[i] = [];
      dp[i][0] = i;
    }
  
    for (let j = 0; j <= len2; j++) {
      dp[0][j] = j;
    }
  
    // Fill in the table based on the edit operations required
    for (let i = 1; i <= len1; i++) {
      for (let j = 1; j <= len2; j++) {
        const cost = s1[i - 1] === s2[j - 1] ? 0 : 1;
        dp[i][j] = Math.min(
          dp[i - 1][j] + 1, // Deletion
          dp[i][j - 1] + 1, // Insertion
          dp[i - 1][j - 1] + cost // Substitution
        );
      }
    }
  
    // Return the edit distance between the two strings
    return dp[len1][len2];
  };
  