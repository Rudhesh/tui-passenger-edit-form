import {
  Modal,
  Box,
  Typography,
  Container,
  Accordion,
  AccordionDetails,
  AccordionSummary,
} from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import GroupIcon from "@mui/icons-material/Group";
import ConfirmationPage from "./components/ConfirmationPage";
import PassengerForm from "./components/PassengerForm";
import passengersData from "./passengers.json";
import { useEffect, useState } from "react";
import { Passenger } from "./types/types";

function App() {
  const [initialPassenger, setInitialPassenger] = useState<Passenger>(
    passengersData[0]
  );
  const [proposedPassenger, setProposedPassenger] = useState(initialPassenger);
  const [selectedPassengerKey, setSelectedPassengerKey] = useState<string>("0");
  const [showConfirmation, setShowConfirmation] = useState(false);
  const [editable, setEditable] = useState(true);
  const [expanded, setExpanded] = useState(false);

  useEffect(() => {
    setInitialPassenger(
      passengersData[selectedPassengerKey as keyof typeof passengersData]
    );
    
  }, [selectedPassengerKey]);


  const onUpdatePassenger = (updatedPassenger: Passenger) => {
    setProposedPassenger(updatedPassenger);
    setShowConfirmation(true);
    setExpanded(true);
  };

  const handleConfirmChanges = () => {
    setInitialPassenger(proposedPassenger);
    setShowConfirmation(false);
    setEditable(false);
  };

  const handleCancelUpdate = () => {
    setShowConfirmation(false);
    setEditable(true);
    setExpanded(true);
  };
  const handleSelectPassenger = (event: React.ChangeEvent<HTMLSelectElement>) => {
    // Set the selected passenger key
    setSelectedPassengerKey(event.target.value);
  };

  const modalStyle = {
    position: "absolute" as "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  const adultCount = Object.values(passengersData).reduce((count, passenger) => {
    if (passenger.title === "MR" || passenger.title === "MRS") {
      return count + 1;
    }
    return count;
  }, 0);
  
  const childCount = Object.values(passengersData).length - adultCount;

  return (
    <div>
      <Container maxWidth="lg">
        <Accordion
          expanded={expanded}
          onChange={() => setExpanded(!expanded)}
          sx={{ border: "2px solid black", marginTop: "30px" }}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel3-content"
            id="panel3-header"
          >
            <Box
              sx={{
                backgroundColor: "#cfe8fc",
                padding: "5px",
                borderRadius: "50px",
                display: "flex",
                alignItems: "center",
                marginRight: "10px",
              }}
            >
              <GroupIcon />
            </Box>{" "}
            <Box  sx={{ display: "flex", color: "#191787"}}>
            <Typography sx={{ display: "flex", alignItems: "center" }}>
              PASSENGER DETAILS
            </Typography>
            <Typography sx={{ display: "flex", alignItems: "center", mr: 2, ml: 2 }}>
  {adultCount} Adult
</Typography>
<Typography sx={{ display: "flex", alignItems: "center", mr: 2, ml: 2 }}>
  {childCount} Child
</Typography>
<Typography sx={{ display: "flex", alignItems: "center", mr: 2, ml: 2 }}>
  0 Infant
</Typography>
</Box>
          </AccordionSummary>
          <AccordionDetails sx={{ bgcolor: "#cfe8fc", padding: "10px" }}>
            <PassengerForm
              initialPassenger={initialPassenger}
              onUpdatePassenger={onUpdatePassenger}
              editable={editable}
              handleSelectPassenger = {handleSelectPassenger}
              selectedPassengerKey = {selectedPassengerKey}
            />
          </AccordionDetails>
        </Accordion>

        <Modal
          open={showConfirmation}
          onClose={handleCancelUpdate}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={modalStyle}>
            <ConfirmationPage
              proposedPassenger={proposedPassenger}
              onCancel={handleCancelUpdate}
              onConfirm={handleConfirmChanges}
            />
          </Box>
        </Modal>
      </Container>
    </div>
  );
}

export default App;
