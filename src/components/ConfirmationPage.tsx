import React from 'react';
import { ConfirmationPageProps } from "../types/types";
import { Box, Button, Divider, List, ListItem, ListItemText, Typography } from '@mui/material';

const ConfirmationPage: React.FC<ConfirmationPageProps> = ({ proposedPassenger, onCancel, onConfirm }) => {
  return (
    <Box display="flex" justifyContent="center" flexDirection="column">
      <Typography variant="h6" component="h2" sx={{ mb: 2 }}>
  Please review your changes carefully
</Typography>
<Typography variant="body1" sx={{ mb: 2 }}>
  You are allowed to make changes only once. Ensure that all details are correct before confirming.
</Typography>
      <Divider />
      <List>
        <ListItem>
          <ListItemText primary={`Title: ${proposedPassenger.title}`} />
        </ListItem>
        <ListItem>
          <ListItemText primary={`First Name: ${proposedPassenger.firstName}`} />
        </ListItem>
        <ListItem>
          <ListItemText primary={`Last Name: ${proposedPassenger.lastName}`} />
        </ListItem>
        <ListItem>
          <ListItemText primary={`Gender: ${proposedPassenger.gender}`} />
        </ListItem>
        <ListItem>
          <ListItemText primary={`Date of Birth: ${proposedPassenger.dateOfBirth}`} />
        </ListItem>
      </List>
      <Box sx={{ display: 'flex', justifyContent: 'flex-end', width: '100%', mt: 2,  }}>
        <Button onClick={onCancel} sx={{ mr: 1 }}>Cancel</Button>
        <Button onClick={onConfirm}  sx={{ bgcolor: '#191787', '&:hover': { bgcolor: '#191787' } }} variant="contained">Confirm Changes</Button>
      </Box>
    </Box>
  );
};

export default ConfirmationPage;
