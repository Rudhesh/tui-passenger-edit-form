import React, { useEffect, useState } from "react";
import { Passenger, PassengerFormProps } from "../types/types";
import { calculateEditDistance } from "../utils/utils";
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
} from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import dayjs from "dayjs";

const PassengerForm: React.FC<PassengerFormProps> = ({
  initialPassenger,
  onUpdatePassenger,
  editable,
  handleSelectPassenger,
  selectedPassengerKey,
}) => {
  const [passenger, setPassenger] = useState<Passenger>(initialPassenger);

  useEffect(() => {
    if (initialPassenger) {
      setPassenger(initialPassenger);
    }
  }, [initialPassenger]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;

    // Ensure the field is editable and the value is not empty
    if (editable && value) {
      // Check if the field is not 'firstName' or 'lastName'
      if (name !== "firstName" && name !== "lastName") {
        // For title, gender, and dateOfBirth, directly update the passenger state
        setPassenger((prevPassenger) => ({
          ...prevPassenger,
          [name]: value,
        }));
      } else {
        const originalName = initialPassenger[name];

        // Calculate the edit distance between the original name and the new value
        const distance = calculateEditDistance(originalName, value);
        const maxDistance = 3; // Maximum allowed edit distance

        // Check if the edit distance exceeds the maximum allowed limit
        if (distance <= maxDistance) {
          // Check if the new value is not longer than the original value
          if (value.length <= originalName.length) {
            setPassenger((prevPassenger) => ({
              ...prevPassenger,
              [name]: value,
            }));
          }
        }
      }
    }
  };

  const handleUpdateClick = () => {
    // Pass updated passenger to parent component for updating
    
    onUpdatePassenger(passenger);
   

    
  };
  return (
    <Card
      style={{ padding: "20px" }}
      sx={{
        "& .MuiTextField-root": { m: 1, width: "35ch" },
      }}
    >
      
      <CardContent>
      <Box display="flex" justifyContent="space-between" alignItems="center" sx={{  mb: 3, }}>
        <FormControl variant="standard"  sx={{  m: 1, width: "25ch" }}>
          <InputLabel id="demo-simple-select-standard-label">
           Select Passenger
          </InputLabel>
          <Select
           labelId="passenger-select-label"
           id="passenger-select"       
            value={selectedPassengerKey}
            onChange={handleSelectPassenger}
            label="Passenger"
           readOnly={!editable}
          >
            <MenuItem value={0}>First Passenger</MenuItem>
            <MenuItem value={1}>Second Passenger</MenuItem>
            <MenuItem value={2}>Third Passenger</MenuItem>
          </Select>
        </FormControl>

        <CardActions sx={{ justifyContent: "flex-end" }}>
          <Button
            variant="contained"
            onClick={handleUpdateClick}
            disabled={!editable}
            sx={{ bgcolor: '#191787', '&:hover': { bgcolor: '#191787' } }}
          >
            Update Details
          </Button>
        </CardActions>
      </Box>
        <TextField
          select // Use select for dropdown options
          name="title"
          label="Salutation"
          value={passenger.title}
          onChange={handleChange}
          InputProps={{
            readOnly: !editable,
          }}
          variant="standard"
          style={{ width: "15ch" }}
        >
          <MenuItem value="MR">MR</MenuItem>
          <MenuItem value="MRS">MRS</MenuItem>
          <MenuItem value="BOY">BOY</MenuItem>
          <MenuItem value="GIRL">GIRL</MenuItem>
        </TextField>

        <TextField
          label="First Name"
          name="firstName"
          value={passenger.firstName}
          onChange={handleChange}
          InputProps={{
            readOnly: !editable,
          }}
          helperText={editable ? "Only change up to 3 characters." : null}
          variant="standard"
        />

        <TextField
          label="Last Name"
          name="lastName"
          value={passenger.lastName}
          onChange={handleChange}
          InputProps={{
            readOnly: !editable,
          }}
          helperText={editable ? "Only change up to 3 characters." : null}
          variant="standard"
        />
        <TextField
          select
          name="gender"
          label="Gender"
          value={passenger.gender}
          onChange={handleChange}
          InputProps={{
            readOnly: !editable,
          }}
          variant="standard"
          style={{ width: "15ch" }}
        >
          <MenuItem value="MALE">MALE</MenuItem>
          <MenuItem value="FEMALE">FEMALE</MenuItem>
        </TextField>

        <DatePicker
          label="Birth date"
          value={dayjs(passenger.dateOfBirth)}
          views={["year", "month", "day"]}
          onChange={(date: any) =>
            setPassenger((prevPassenger) => ({
              ...prevPassenger,
              dateOfBirth: dayjs(date).format("YYYY-MM-DD"),
            }))
          }
          slotProps={{
            openPickerIcon: { fontSize: "small" },
            textField: {
              variant: "standard",
              InputProps: {
                readOnly: !editable,
              },
              style: { width: "15ch" },
            },
          }}
        />
      </CardContent>
    </Card>
  );
};

export default PassengerForm;
